//
//  LinkedList.cpp
//
//
//  Created by James Price on 06/10/2016.
//
//

#include "LinkedList.h"
#include <iostream>

LinkedList::LinkedList()
{
    head = nullptr;
}

LinkedList::~LinkedList()
{
    
}

void LinkedList::add (int itemValue)
{
    Node*  n = new Node;
    n->next = nullptr;
    n->value = itemValue;
    
    if(head != nullptr)
    {
        Node* curr = head;
        while(curr ->next != nullptr)
            curr = curr->next;
        
        curr->next = n;
        
    }
    else
    {
        head = n;
    }
    
}

//void LinkedList::remove (int del)
//{
//    
//    Node* delptr = NULL;
//    
//    temp = head;
//    curr = head;
//    while (curr != NULL && curr ->index != del)
//    {
//        temp=curr;
//        curr = curr -> next;
//    }
//    if (curr == NULL)
//    {
//        std::cout << del << "not in list\n";
//        delete delptr;
//    }
//    else
//    {
//        delptr = curr;
//        curr = curr->next;
//        temp->next = curr;
//        
//        delete delptr;
//        
//        std::cout << "the value"  << del << "was deleted";
//        
//    }
//}

float LinkedList::get (int indexNo)
{
    float indexValue;
    
    //Node* temp = head;
    Node* curr = head;
    int index = 0;
    
    while (curr != nullptr && index != indexNo)
    {
        //temp=curr;
        curr = curr -> next;
        index ++;
    }
    
    if (index == indexNo)
    {
        indexValue = curr->value;
        index ++;
    }
    else
    {
        std::cout << "index not found";
    }
    
    return indexValue;
    

}

int LinkedList::size()
{
    
    int size = 0;
    
    if(head != nullptr)
    {
        size = size + 1;
        Node* curr = head;
        while (curr->next != nullptr)
        {
            size = size + 1;
            curr = curr->next;
        }
    }
    
    return size;
}