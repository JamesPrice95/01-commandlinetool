//
//  array.cpp
//  
//
//  Created by James Price on 06/10/2016.
//
//

#include "array.h"
#include <iostream>

Array :: Array()
{
    arraySize = 0;
    floatPoint = nullptr;
    
}

Array :: ~Array()
{
    if (floatPoint != nullptr)
        delete[] floatPoint;
}

void Array :: add (float itemValue)
{
    float* tempPtr = new float[arraySize + 1];
    for (int i = 0; i < arraySize; i++)
        tempPtr[i] = floatPoint[i];
    delete[] floatPoint; 
    floatPoint = tempPtr;
    floatPoint[arraySize] = itemValue;
    arraySize++;
}

float Array ::  get (int index)
{
    float gotValue;
    
    gotValue = floatPoint[index];
    
    return gotValue;
}

int Array :: size()
{
    return  arraySize;
}