//
//  LinkedList.h
//  
//
//  Created by James Price on 06/10/2016.
//
//

#ifndef ____LinkedList__
#define ____LinkedList__

#include <stdio.h>

class LinkedList
{
public:
    
    LinkedList();
    
    ~LinkedList();
    
    void add (int itemValue);
    
//    void remove (int del);
    
    float get (int index);
    
    int size(); 
    
    
private:
    
    struct Node
    {
        int value;
        Node* next;
    };
    
    Node* head;
};



#endif /* defined(____LinkedList__) */
