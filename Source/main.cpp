//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "LinkedList.h"

bool testLinkedList();

int main (int argc, const char* argv[])
{
    LinkedList list;
   // float value;
  //  std::cin >> value;
    
    bool state;
    
   state = testLinkedList();
    
    std::cout << state;
    return 0;
}

bool testLinkedList()
{
    LinkedList list;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);

    if (list.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        list.add (testArray[i]);
        for (int y = 0; y < list.size(); y++)
        {
            std::cout << list.get(y) << "\n";
        }
        if (list.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (list.get (i) != testArray[i])
       {
           std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    return true;

}